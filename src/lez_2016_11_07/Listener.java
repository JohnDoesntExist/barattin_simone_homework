package lez_2016_11_07;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by simone on 08/11/16.
 */
class Listener implements ActionListener{
    JButton b1,b2,b3;
    Ascensore a;

    public Listener(JButton b1, JButton b2, JButton b3, Ascensore a) {
        this.b1=b1;
        this.b2=b2;
        this.b3=b3;
        this.a=a;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==b1){
            try {
                a.setPiano(1);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            notifyAll();
        }
        else if (e.getSource()==b2){
            try {
                a.setPiano(2);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            notifyAll();
        }
        else{
            try {
                a.setPiano(3);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            notifyAll();
        }
    }
}
