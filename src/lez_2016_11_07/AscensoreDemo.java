package lez_2016_11_07;

import javax.swing.*;

/**
 * Created by simone on 07/11/16.
 */
class AscensoreDemo extends JFrame{
    AscensoreDemo(){
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(0,0,500,600);
        setVisible(true);

        Ascensore a=new Ascensore();
        GUI g=new GUI(a);

        add(g);

        a.start();
    }

    public static void main(String args[]){
        new AscensoreDemo();
    }
}
