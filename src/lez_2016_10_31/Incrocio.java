package lez_2016_10_31;

/**
 * Created by simone on 30/10/16.
 */
class Incrocio {
    boolean occupied=false;

    synchronized void occupa() throws InterruptedException {
        while(occupied){
            System.out.println(Thread.currentThread().getName()+" si ferma all'incrocio");
            wait();
        }
        System.out.println(Thread.currentThread().getName()+" occupa l'incrocio");
        occupied=true;
    }

    synchronized void libera() throws InterruptedException {
        occupied=false;
        notifyAll();
    }
}
