package lez_2016_10_31_classe;

import javax.swing.*;
import java.awt.*;

/**
 * Created by simone on 31/10/16.
 */
class IncrocioDemo extends JFrame{
    int n,c=0;

    IncrocioDemo() throws HeadlessException{
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(0,0,500,500);
        setResizable(false);
        setVisible(true);

        Incrocio i=new Incrocio();
        GUI g=new GUI(i);
        add(g);

        while(true) {
            n = (int) (Math.random() * 5);
            Macchina m = new Macchina(i, n);

            m.setName("M" + Integer.toString(c));
            g.add(m);
            m.start();
            c++;
            try{
                Thread.sleep((int)(Math.random()*5000));
            }catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }

    public static void main(String args[]){
        new IncrocioDemo();
    }
}
