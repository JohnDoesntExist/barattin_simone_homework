package lez_2016_10_31_classe;

/**
 * Created by simone on 31/10/16.
 */
class Incrocio {
    int N=0,E=0,S=0,W=0;

    public synchronized void checkRight(int s) throws InterruptedException {
        if(s==1){
            while(W!=0){
                System.out.println("La strada a destra e' occupata. "+Thread.currentThread().getName()+" si ferma.");
                wait();
            }
            System.out.println(Thread.currentThread().getName()+" sta passando l'incrocio.");
        }
        else if(s==2){
            while(N!=0){
                System.out.println("La strada a destra e' occupata. "+Thread.currentThread().getName()+" si ferma.");
                wait();
            }
            System.out.println(Thread.currentThread().getName()+" sta passando l'incrocio.");
        }
        if(s==3){
            while(E!=0){
                System.out.println("La strada a destra e' occupata. "+Thread.currentThread().getName()+" si ferma.");
                wait();
            }
            System.out.println(Thread.currentThread().getName()+" sta passando l'incrocio.");
        }
        if(s==4){
            while(S!=0){
                System.out.println("La strada a destra e' occupata. "+Thread.currentThread().getName()+" si ferma.");
                wait();
            }
            System.out.println(Thread.currentThread().getName()+" sta passando l'incrocio.");
        }
    }

    public synchronized void occupa(int s) {
        if(s==1)
            N++;
        else if(s==2)
            E++;
        else if(s==3)
            S++;
        else
            W++;
    }

    public synchronized void libera(int s) {
        if(s==1)
            N--;
        else if(s==2)
            E--;
        else if(s==3)
            S--;
        else
            W--;
        notifyAll();
    }
}
