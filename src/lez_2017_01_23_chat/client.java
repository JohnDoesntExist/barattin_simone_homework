package lez_2017_01_23_chat;

import javax.swing.JFrame;
import javax.swing.JTextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class client {
	JFrame frame;
	private JTextField textField,textField_1,txtLocalhost;
	private JLabel lblPort,lblAddress; 
	private JButton btnNewButton,btnSend;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JLabel lblUsername;
	private JTextField textField_2;
	private Socket socket;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	
	public client(){
		setupGui();
	}
	
	private void setupGui() {
		frame=new JFrame("Client Chat");
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(12, 292, 284, 19);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		btnSend = new JButton("Send");
		btnSend.setBounds(308, 286, 70, 25);
		frame.getContentPane().add(btnSend);
		btnSend.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String message=(String)textField.getText();
					if(!message.equals(""))
						sendMessage(message);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		lblAddress = new JLabel("Address");
		lblAddress.setBounds(12, 12, 70, 15);
		frame.getContentPane().add(lblAddress);
		
		txtLocalhost = new JTextField();
		txtLocalhost.setText("localhost");
		txtLocalhost.setEditable(false);
		txtLocalhost.setBounds(81, 10, 70, 19);
		frame.getContentPane().add(txtLocalhost);
		txtLocalhost.setColumns(10);
		
		lblPort = new JLabel("Port");
		lblPort.setBounds(169, 12, 40, 15);
		frame.getContentPane().add(lblPort);
		
		textField_1 = new JTextField();
		textField_1.setText("54321");
		textField_1.setBounds(207, 10, 50, 19);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		btnNewButton = new JButton("Connect");
		btnNewButton.setBounds(269, 7, 94, 25);
		btnNewButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				connectServer(txtLocalhost, textField_1,textField_2);
			}
		});
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		
		scrollPane=new JScrollPane(textArea);
		scrollPane.setBounds(12,60, 360, 210);
		frame.getContentPane().add(scrollPane);
		
		lblUsername = new JLabel("Username");
		lblUsername.setBounds(12, 33, 81, 15);
		frame.getContentPane().add(lblUsername);
		
		textField_2 = new JTextField();
		textField_2.setBounds(91, 31, 118, 19);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		frame.getContentPane().add(btnNewButton);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setBounds(100,100,390,350);
	}

	protected void connectServer(JTextField txtLocalhost2, JTextField textField_12, JTextField txt2) {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					socket = new Socket( txtLocalhost.getText(), Integer.parseInt(textField_1.getText()));
					output=new ObjectOutputStream(socket.getOutputStream());
					input=new ObjectInputStream(socket.getInputStream());
					textArea.append("Got streams");
					output.writeObject(txt2.getText());
					output.flush();
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
		Thread listener = new Thread(new Listener());
		listener.start();
	}
	
	protected void sendMessage(String text) throws IOException {
		output.writeObject(text);
		output.flush();
	}
	
	

	public static void main(String argss[]){
		new client();
	}
	
	public class Listener extends Thread{

		@Override
		public void run() {
			String message;
			try {
				sleep(1000);
				while(true){
					message=(String)input.readObject();
					textArea.append(message+"\n");
				}
			} catch (ClassNotFoundException | IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}
