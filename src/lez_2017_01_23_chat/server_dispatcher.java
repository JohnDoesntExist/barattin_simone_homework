package lez_2017_01_23_chat;

import javax.swing.JFrame;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.JTextArea;
import javax.swing.JScrollBar;

public class server_dispatcher{	
	
	public class manageUser implements Runnable{
		private Socket socket;
		private String user;
		private ObjectOutputStream[] output;
		private ObjectInputStream input;
		private String message;
		private int i;
		
		public manageUser(int i, ObjectOutputStream[] output, ObjectInputStream input) {
			this.i=i;
			this.output=output;
			this.input=input;		
		}

		@Override
		public void run() {
			while(true){
				try {
					message=(String)input.readObject();
					textArea.append(users[i]+">>"+message+"\n");
					tellUsers(users[i]+">>"+message,i);
				} catch (ClassNotFoundException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
	private JFrame frame;
	private JTextArea textArea;
	private JLabel lblServerPort,lblServerAddress;
	private JButton btnStart, btnStop; 
	private JScrollPane scrollPane;
	private ObjectOutputStream[] outstreams;
	private String[] users;
	private String message;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private ServerSocket serversocket;
	private Socket socket;
	private int counter;
	
	public server_dispatcher() {
		setupGui();
		
	}
	
	public synchronized void tellUsers(String string, int c) throws IOException {
		message=string;
		textArea.append("counter = "+counter);
		for(int i=0;i<counter;i++){
			if(i!=c){
				textArea.append("Messaggio inviato a "+users[i]+"\n");
				outstreams[i].writeObject(string);
				outstreams[i].flush();
				textArea.append("\nMessaggio iniato\n");
			}
		}
	}

	public static void main(String args[]){
		new server_dispatcher();
	}
	
	void setupGui(){
		frame=new JFrame("Server-Dispatcher");
		frame.getContentPane().setLayout(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setBounds(100,100,390,300);
		
		btnStart = new JButton("Start");
		btnStart.setBounds(247, 202, 117, 25);
		btnStart.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					startServer();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		frame.getContentPane().add(btnStart);
		
		btnStop = new JButton("Stop");
		btnStop.setBounds(247, 236, 117, 25);
		btnStop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				stopServer();
			}
		});
		frame.getContentPane().add(btnStop);
		
		lblServerPort = new JLabel("Server port: 54321");
		lblServerPort.setBounds(12, 207, 137, 15);
		frame.getContentPane().add(lblServerPort);
		
		lblServerAddress = new JLabel("Server address: localhost");
		lblServerAddress.setBounds(12, 241, 189, 15);
		frame.getContentPane().add(lblServerAddress);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		
		scrollPane=new JScrollPane(textArea);
		scrollPane.setBounds(12, 12, 360, 180);
		frame.getContentPane().add(scrollPane);
	}

	protected void stopServer() {
		textArea.append("Chiusura server in corso...");
		try{
			for(int i=0;i<counter;i++){
				outstreams[i].close();
			}
		}catch (IOException e) {
			// TODO: handle exception
		}
	}

	protected void startServer() throws IOException {
		btnStart.setEnabled(false);
		btnStop.setEnabled(true);
		serversocket=new ServerSocket(54321);
		
		new Thread() {
			
			@Override
			public void run() {
				outstreams=new ObjectOutputStream[30];
				users=new String[30];
				String user;
				counter=0;
				while(true){
					user="";
					textArea.append("Server in attesa sulla porta 54321\n");
					try {
						socket=serversocket.accept();
						textArea.append("Nuova connesione alla chat\n");
						outstreams[counter]=new ObjectOutputStream(socket.getOutputStream());
						input=new ObjectInputStream(socket.getInputStream());
						try {
							users[counter]=(String)input.readObject();
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						textArea.append(users[counter]+" connesso al server\n");
						new Thread(new manageUser(counter,outstreams,input)).start();
						System.out.println("Fine ciclo");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}/* catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					counter++;
				}
			}
		}.start();
	}
}
