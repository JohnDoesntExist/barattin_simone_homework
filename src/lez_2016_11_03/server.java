package lez_2016_11_03;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class server {

	private ObjectOutputStream output;
	private ObjectInputStream input;
	private ServerSocket server;
	private Socket connection;

	private int port = 12345;
	private int number = (int)(Math.random()*100);
	private boolean t=false;

	
	public server() {
		System.out.println(number);
		
		try {

			// Step 1: Create a ServerSocket.
			server = new ServerSocket(port);

			//step 2: attesa connessione
			String message = "server in attesa sulla porta " + port  ;
			System.out.println(message);
			connection = server.accept(); //il programma si blocca
			
			message =  "Connesso con " +
					connection.getInetAddress().getHostName();
			System.out.println(message);

			//step 3: ottenere gli stream
			output = new ObjectOutputStream(connection.getOutputStream() );
			input = new ObjectInputStream(connection.getInputStream());

			System.out.println("Stream ottenuti");

			//step 4: comunicazione
			
			//lettura password
			System.out.println("attesa numero");
			int n;
			do{ 
			n= (int) input.readObject();
			System.out.println("Letto numero: " + n);
			
			System.out.println("invio risposta");
			if(n==number) {
				output.writeObject("numero corretto");
				output.flush();
				t=true;
				System.out.println("entra");

			} else {
				output.writeObject("numero errato");
				output.flush();
			}	
			}while(t!=true);
		}

		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			// Step 5: chiusura della connessione
			System.out.println("chiusura connessione");
			try {
				connection.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				if(output != null) output.close();
			}
			catch( IOException ioException ) {
				ioException.printStackTrace();
			}   


			try {
				if(input != null)input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}


	} // end Server constructor

	public static void main( String args[] )
	{
		new server();

	}

}  // end class Server
