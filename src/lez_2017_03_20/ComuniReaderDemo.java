package lez_2017_03_20;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.JOptionPane;

public class ComuniReaderDemo {

	public static void main(String[] args) {
		File file = new File("src/lez_2017_03_20/listacomuni.txt");
		Scanner sc;
		String text = null;
		Vector<String> tokens = new Vector<>();
		try {
			sc = new Scanner(file, "Cp1252");
			sc.useDelimiter("\n"); 
			int count = 0;
			while (sc.hasNext()) {
				
				text = sc.next();			
				
				Scanner scanner = new Scanner(text);
				scanner.useDelimiter(";");
				
				scanner.next();	
				tokens.add(scanner.next());
				count++;
				scanner.close();
				
			}
			sc.close(); 
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		for (int i = 0; i < tokens.size(); i++) {
			System.out.println(tokens.elementAt(i));
		}
		
		

	}

}
