package lez_2017_03_20;

// Demonstrate check boxes. 
  
import java.awt.*;  
import java.awt.event.*;  
import javax.swing.*;  
   
public class CBDemo implements ItemListener {  
  
  JLabel jlabOptions;   
  JLabel jlabWhat;   
  JLabel jlabChange;   
  JCheckBox jcbOptions; 
  JCheckBox jcbSpeed; 
  JCheckBox jcbSize; 
  JCheckBox jcbDebug; 
  
  CBDemo() {  
    // Create a new JFrame container.  
    JFrame jfrm = new JFrame("Demonstrate Check Boxes");  
  
    // Specify a 1 column, 7 row grid layout. 
    jfrm.getContentPane().setLayout(new GridLayout(7, 1)); 
  
    // Give the frame an initial size.  
    jfrm.setSize(300, 150);  
  
    // Terminate the program when the user closes the application.  
    jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
  
    // Create two labels. 
    jlabOptions = new JLabel("Options:");  
    jlabChange = new JLabel("");  
    jlabWhat = new JLabel("Options selected:");  
  
    // Make check boxes. 
    jcbOptions = new JCheckBox("Enable Options");  
    jcbSpeed = new JCheckBox("Maximize Speed");  
    jcbSize = new JCheckBox("Minimize Size");  
    jcbDebug = new JCheckBox("Debug");  
 
    // Initialy disable option check boxes. 
    jcbSpeed.setEnabled(false); 
    jcbSize.setEnabled(false); 
    jcbDebug.setEnabled(false); 
 
    // Add item listener for jcbOptions. 
    jcbOptions.addItemListener(new ItemListener() { 
      public void itemStateChanged(ItemEvent ie) { 
        if(jcbOptions.isSelected()) { 
          jcbSpeed.setEnabled(true); 
          jcbSize.setEnabled(true); 
          jcbDebug.setEnabled(true); 
        } 
        else { 
          jcbSpeed.setEnabled(false); 
          jcbSize.setEnabled(false); 
          jcbDebug.setEnabled(false); 
        } 
      } 
    }); 
 
    // Events generated by the Options check boxes 
    // are handled in common by the itemStateChanged() 
    // method implemented by CBDemo. 
    jcbSpeed.addItemListener(this); 
    jcbSize.addItemListener(this); 
    jcbDebug.addItemListener(this); 
  
    // Add checkboxes and labels to the content pane.  
    jfrm.getContentPane().add(jcbOptions);   
    jfrm.getContentPane().add(jlabOptions);  
 
    jfrm.getContentPane().add(jcbSpeed);   
    jfrm.getContentPane().add(jcbSize);   
    jfrm.getContentPane().add(jcbDebug);   
    jfrm.getContentPane().add(jlabChange);  
    jfrm.getContentPane().add(jlabWhat);  
  
    // Display the frame.  
    jfrm.setVisible(true);  
  }  
 
  // This is the handler for all of the Options check boxes.   
  public void itemStateChanged(ItemEvent ie) { 
    String opts = ""; 
 
    // Obtain a reference to the check box that 
    // caused the event. 
    JCheckBox cb = (JCheckBox) ie.getItem(); 
 
    // Tell the user what they did. 
    if(ie.getStateChange() == ItemEvent.SELECTED)  
      jlabChange.setText("Selection change: " + 
                          cb.getText() + " selected."); 
    else 
      jlabChange.setText("Selection change: " + 
                         cb.getText() + " cleared."); 
 
    // Build a string that contains all selected options. 
    if(jcbSpeed.isSelected()) opts += "Speed "; 
    if(jcbSize.isSelected()) opts += "Size "; 
    if(jcbDebug.isSelected()) opts += "Debug "; 
 
    // Display the currently selected options. 
    jlabWhat.setText("Options selected: " + opts); 
  } 
 
 
  public static void main(String args[]) {  
    // Create the frame on the event dispatching thread.  
    SwingUtilities.invokeLater(new Runnable() {  
      public void run() {  
        new CBDemo();  
      }  
    });  
  }  
}
