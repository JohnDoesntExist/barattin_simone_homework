package lez_2017_03_20;
//Demonstrate a simple combo box. 

import javax.swing.*;  
import java.awt.*; 
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Vector; 

class ComboBoxDemo {  

	JComboBox jcbb; 
	JLabel jlab; 

	// Create an array of apple varieties. 
	Vector<String> comuni = null; 

	ComboBoxDemo() {  
		// Create a new JFrame container. 
		JFrame jfrm = new JFrame("JComboBox Demo");  

		// Specify FlowLayout manager.   
		jfrm.getContentPane().setLayout(new FlowLayout());  

		// Give the frame an initial size.  
		jfrm.setSize(220, 240);  

		// Terminate the program when the user closes the application.  
		jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  

		// Create a JComboBox 
		comuni= getComuni();
		jcbb = new JComboBox(comuni); 
		
		// Make a label that displays the selection. 
		jlab = new JLabel(); 

		// Add action listener for the combo box. 
		jcbb.addActionListener(new ActionListener() {  
			public void actionPerformed(ActionEvent le) {  
				// Get a reference to the item selected. 
				String item = (String) jcbb.getSelectedItem(); 

				// Display the selected item. 
				jlab.setText("Current selection " + item); 
			}  
		});  
		
//		Ascoltatore ascoltatore = new Ascoltatore();
//		jcbb.addActionListener(ascoltatore);

		// Initially select the first item in the list. 
		jcbb.setSelectedIndex(0); 

		// Add the combo box and label to the content pane. 
		jfrm.getContentPane().add(jcbb); 
		jfrm.getContentPane().add(jlab); 

		// Display the frame.  
		jfrm.setVisible(true);  
	}  
	
	private Vector<String> getComuni() {
		File file = new File("src/lez_2017_03_20/listacomuni.txt");
		Scanner sc;
		String text = null;
		Vector<String> tokens = new Vector<>();
		try {
			sc = new Scanner(file, "Cp1252");
			sc.useDelimiter("\n"); 
			int count = 0;
			while (sc.hasNext()) {
				
				text = sc.next();			
				
				Scanner scanner = new Scanner(text);
				scanner.useDelimiter(";");
				
				scanner.next();	
				tokens.add(scanner.next());
				count++;
				scanner.close();
				
			}
			sc.close(); 
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return tokens;
		}

//	
//	class Ascoltatore implements ActionListener {
//
//		@Override
//		public void actionPerformed(ActionEvent e) {
//			// TODO Auto-generated method stub
//			
//		}
//		
//	}
//	
	

	public static void main(String args[]) {  
		// Create the frame on the event dispatching thread.  
		SwingUtilities.invokeLater(new Runnable() {  
			public void run() {  
				new ComboBoxDemo();  
			}  
		});   
	}  
}

