package lez_2017_03_13;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JTextArea;

public class Demo {
	JFrame frame;
	JTabbedPane tp;
	JPanel p1,p2,p3;
	String classi[];
	Vector<String> alunni=new Vector<>();
	int i=0;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	
	public Demo(DatabaseStudente databaseStudente,Vector classe) {
		frame=new JFrame();
		tp=new JTabbedPane();
		p1=new JPanel();
		p2=new JPanel();
		p3=new JPanel();
		frame.getContentPane().add(tp);
		tp.addTab("Classe", p1);
		tp.addTab("Studente", p2);
		tp.addTab("Visulizza", p3);
		
		p1.setLayout(null);
		
		JLabel lblClasse = new JLabel("Classe");
		lblClasse.setBounds(115, 36, 70, 15);
		p1.add(lblClasse);
		
		JLabel lblSezione = new JLabel("Sezione");
		lblSezione.setBounds(115, 93, 70, 15);
		p1.add(lblSezione);
		
		JLabel lblIndirizzo = new JLabel("Indirizzo");
		lblIndirizzo.setBounds(115, 143, 70, 15);
		p1.add(lblIndirizzo);
		
		JButton btnSalva = new JButton("Send");
		btnSalva.setBounds(31, 269, 117, 25);
		btnSalva.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					databaseStudente.sendDataClasse(textField_3.getText(),textField_4.getText(),textField_5.getText());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		p1.add(btnSalva);
		
		JButton btnReset = new JButton("Reset");
		btnReset.setBounds(241, 269, 117, 25);
		btnReset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				textField_3.setText("");
				textField_4.setText("");
				textField_5.setText("");
			}
		});
		p1.add(btnReset);
		
		textField_3 = new JTextField();
		textField_3.setBounds(192, 34, 114, 19);
		p1.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(192, 91, 47, 19);
		p1.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(192, 141, 153, 19);
		p1.add(textField_5);
		textField_5.setColumns(10);
		
		p2.setLayout(null);
		
		JLabel lblCognome = new JLabel("Cognome");
		lblCognome.setBounds(12, 12, 78, 15);
		p2.add(lblCognome);
		
		textField = new JTextField();
		textField.setBounds(140, 10, 198, 19);
		p2.add(textField);
		textField.setColumns(10);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(12, 43, 70, 15);
		p2.add(lblNome);
		
		textField_1 = new JTextField();
		textField_1.setBounds(140, 41, 198, 19);
		p2.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblDataDiNascita = new JLabel("Data di nascita");
		lblDataDiNascita.setBounds(12, 74, 110, 15);
		p2.add(lblDataDiNascita);
		
		textField_2 = new JTextField();
		textField_2.setBounds(140, 72, 198, 19);
		p2.add(textField_2);
		textField_2.setColumns(10);
		
		JCheckBox chckbx = new JCheckBox("Nazionalità estera");
		chckbx.setBounds(102, 99, 157, 23);
		p2.add(chckbx);
		
		JLabel lblComune = new JLabel("Comune");
		lblComune.setBounds(12, 171, 70, 15);
		p2.add(lblComune);
		
		JLabel lblNazionalit = new JLabel("Nazionalità");
		lblNazionalit.setBounds(12, 130, 93, 15);
		p2.add(lblNazionalit);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setEnabled(false);
		comboBox.setBounds(140, 130, 198, 24);
		p2.add(comboBox);
		
		Vector<String> s=getComuni();
		JComboBox comboBox_1 = new JComboBox(s);
		comboBox_1.setBounds(140, 166, 198, 24);
		p2.add(comboBox_1);
		
		JButton btnSend = new JButton("Send");
		btnSend.setBounds(51, 269, 117, 25);
		btnSend.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					databaseStudente.sendDataStudente(textField.getText(), textField_1.getText(), textField_2.getText(), (String)comboBox_1.getSelectedItem());
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		p2.add(btnSend);
		
		JButton btnReset_1 = new JButton("Reset");
		btnReset_1.setBounds(221, 269, 117, 25);
		btnReset_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				textField.setText("");
				textField_1.setText("");
				textField_2.setText("");
				comboBox_1.setSelectedIndex(0);;
			}
		});
		p2.add(btnReset_1);
		
		JLabel lblClasse_1 = new JLabel("Classe");
		lblClasse_1.setBounds(12, 214, 70, 15);
		p2.add(lblClasse_1);
		
		JComboBox comboBox_5 = new JComboBox(classe);
		comboBox_5.setBounds(140, 209, 198, 24);
		p2.add(comboBox_5);
		
		chckbx.addItemListener(new ItemListener() {
		@Override
		public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				if(!chckbx.isSelected()){
					comboBox.setEnabled(false);
					comboBox_1.setEnabled(true);
				}
				else{
					comboBox.setEnabled(true);
					comboBox_1.setEnabled(false);
				}
			}
		});
		
		p3.setLayout(null);
		
		JLabel lblClasse_2 = new JLabel("Classe");
		lblClasse_2.setBounds(33, 23, 70, 15);
		p3.add(lblClasse_2);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(33, 69, 317, 204);
		p3.add(textArea);
		
		JComboBox comboBox_2 = new JComboBox(classe);
		comboBox_2.setBounds(153, 18, 192, 24);
		comboBox_2.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				String sel=(String)comboBox_2.getSelectedItem();
				try {
					ResultSet rs=databaseStudente.getStudenti(sel);
					String r="";
					while(rs.next()){
						r+=rs.getString("nome");
						r+=" "+rs.getString("cognome");
						r+="\n";
					}
					textArea.setText(r);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		

		p3.add(comboBox_2);
		
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(390, 360);
	}
	
	private Vector<String> getComuni() {
		File file = new File("src/lez_2017_03_20/listacomuni.txt");
		Scanner sc;
		String text = null;
		Vector<String> tokens = new Vector<>();
		try {
			sc = new Scanner(file, "Cp1252");
			sc.useDelimiter("\n"); 
			int count = 0;
			while (sc.hasNext()) {
				
				text = sc.next();			
				
				Scanner scanner = new Scanner(text);
				scanner.useDelimiter(";");
				
				scanner.next();	
				tokens.add(scanner.next());
				count++;
				scanner.close();
				
			}
			sc.close(); 
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return tokens;
	}
}
