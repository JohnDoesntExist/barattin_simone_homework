package lez_2017_03_13;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

class DatabaseStudente {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost/scuola_java";
	static final String USER = "root";
	static final String PASS = "";
	Vector<String> classe=new Vector<>();
	private Connection conn = null;
	private Statement stmt = null;
	
	public DatabaseStudente() throws SQLException{
		String result="",sql="";
		conn=DriverManager.getConnection(DB_URL,USER,PASS);
		stmt=conn.createStatement();
		sql="SELECT * FROM classe";
		ResultSet rs=stmt.executeQuery(sql);
		while(rs.next()){
			String res="";
			int id  = rs.getInt("id");	
			res+= rs.getString("classe");
			res+=rs.getString("sezione");
			res+=rs.getString("indirizzo");
			classe.add(res);
		}
		new Demo(this,classe);
	    stmt.close();
	    conn.close();
	}
	
	public static void main(String[] args) {
		try {
			new DatabaseStudente();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void sendDataStudente(String nome,String cognome,String data,String nascita) throws SQLException{
		String result="",sql="";
		conn=DriverManager.getConnection(DB_URL,USER,PASS);
		stmt=conn.createStatement();
		sql="INSERT INTO studente (nome, cognome, data_nascita, comune_nascita, nazione_nascita, fk_classe) "
				+ "VALUES ('"+nome+"','"+cognome+"','"+data+"','"+nascita+"','NULL',0);";
		boolean rs=stmt.execute(sql);
	    stmt.close();
	    conn.close();
	}
	
	public void sendDataClasse(String classe,String sezione,String indirizzo) throws SQLException{
		String result="",sql;
		conn=DriverManager.getConnection(DB_URL,USER,PASS);
		stmt=conn.createStatement();
		sql="INSERT INTO classe (classe, sezione, indirizzo, anno) "
				+ "VALUES ('"+classe+"','"+sezione+"','"+indirizzo+"','2016/2017');";
		boolean rs=stmt.execute(sql);
	    stmt.close();
	    conn.close();	
	}

	public ResultSet getStudenti(String sel) throws SQLException {
		String sql;
		conn=DriverManager.getConnection(DB_URL,USER,PASS);
		stmt=conn.createStatement();
		sql="SELECT nome,cognome FROM studente WHERE fk_classe=(SELECT id FROM classe WHERE classe+sezione+indirizzo= '"+sel+"');";
		ResultSet rs = stmt.executeQuery(sql);
		return rs;		
	}
}
