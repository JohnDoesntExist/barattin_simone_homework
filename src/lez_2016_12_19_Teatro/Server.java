package lez_2016_12_19_Teatro;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private ServerSocket serversocket;
	private Socket socket;
	private String message,initialmsg;
	private int sceltaClient;
	private int postiDisponibili[]=new int[300],postiOccupati[]=new int[300],postiScelti[];
	private int[][] prenotazioni=new int[300][300];
	private String clienti[]=new String[300];
	private String current;
	private boolean t,t1=false,find=false;
	private int i,j,port=54321,k=0;
	
	Server() {
		i=0;
		while(i<postiDisponibili.length){
			postiDisponibili[i]=1;
			postiOccupati[i]=0;
			i++;
		}
		try {
			sceltaClient=0;
			serversocket=new ServerSocket(port);
			System.out.println("Server in attesa sulla porta "+port);
			socket=serversocket.accept();
			System.out.println("Connesso con "+socket.getInetAddress().getHostAddress());
			output=new ObjectOutputStream(socket.getOutputStream());
			input=new ObjectInputStream(socket.getInputStream());
			System.out.println("Stream ottenuti");
			
			initialmsg="0)Disconnetti\n1)Prenota posti a sedere\n2)Disdici prenotazione\n3)Controlla la tua prenotazione"
					+ "\n4)Numero di posti liberi";
			current=(String)input.readObject();
			i=0;
			while(i<clienti.length && !current.equals(clienti[i])){
				if(current.equals(clienti[i])){
					j=0;
					while(j<prenotazioni.length && prenotazioni[i][j]!=0){
						postiScelti[j]=prenotazioni[i][j];
						j++;
					}
				}
				i++;	
			}
			clienti[k]=current;
			output.writeObject(t1);
			output.flush();
			System.out.println(clienti[k]);
			while(socket!=null){
				output.writeObject(initialmsg);
				output.flush();
				System.out.println("flushato initial msg");
				sceltaClient=(int)input.readObject();
				switch (sceltaClient) {
				case 0:	
					System.out.println("Scelta client: "+sceltaClient);
					postiScelti=null;
					t1=false;
					input.close();
					output.close();
					socket.close();
					System.out.println("Aspettando client");
					socket=serversocket.accept();
					System.out.println("connesso a client");
					output=new ObjectOutputStream(socket.getOutputStream());
					input=new ObjectInputStream(socket.getInputStream());
					System.out.println("Stream ottenuti");
					k++;
					current=(String)input.readObject();
					System.out.println(current);
					i=0;
					System.out.println(clienti.length);
					//clienti[k]=current;
					while(i<clienti.length){
						if(current.equals(clienti[i])){
							j=0;
							while(j<prenotazioni.length && prenotazioni[i][j]!=0){
								postiScelti[j]=prenotazioni[i][j];
								j++;
							}
							find=true;
							t1=true;
							break;
						}
						i++;	
					}
					if(find==false)
						clienti[k]=current;
					output.writeObject(t1);
					output.flush();
					
					break;
				case 1:
					System.out.println("Scelta client: "+sceltaClient);
					postiScelti=(int[])input.readObject();
					System.out.println("Array posti ottenuto");
					i=0;
					t=true;
					while(i<postiScelti.length){
						if(postiOccupati[postiScelti[i]]==0){
							postiDisponibili[postiScelti[i]]=0;
							postiOccupati[postiScelti[i]]=1;
							prenotazioni[k][i]=postiScelti[i];
						}
						else{
							t=false;
						}
						i++;
					}
					if(t){
						message="Posti prenotati, se eseguirai un'altra prenotazione questa non potra' piu' essere disdetta.";
					}
					else{
						i=0;
						while(i<postiScelti.length){
							if(postiOccupati[postiScelti[i]]==1){
								postiDisponibili[postiScelti[i]]=1;
								postiOccupati[postiScelti[i]]=0;
							}
							i++;
						}
						message="Alcuni posti sono gia' occupati. Prenotazione annullata.";
					}
					System.out.println(message);
					output.writeObject(message);
					output.flush();
					System.out.println("message inviato");
					break;
					
				case 2:
					t1=(boolean)input.readObject();
					if(t1){
						i=0;
						while(i<postiScelti.length){
							if(postiOccupati[postiScelti[i]]==1){
								postiDisponibili[postiScelti[i]]=1;
								postiOccupati[postiScelti[i]]=0;
							}
							i++;
						}
						message="Prenotazione disdetta con successo";
					}
					else{
						message="Non hai ancora effettutato nessuna prenotazione.";
					}
					output.writeObject(message);
					output.flush();
					break;
					
				case 3:
					t1=(boolean)input.readObject();
					if(t1){
						i=0;
						message="";
						while(i<postiScelti.length){
							message+=""+(postiScelti[i]+1)+" ";
							i++;
						}
					}
					else{
						message="Non hai ancora effettuato nessuna prenotazione";
					}
					output.writeObject(message);
					output.flush();
					break;
				case 4:
					i=0;
					message="";
					while(i<postiDisponibili.length){
						if(postiDisponibili[i]==1)
							message+=""+(i+1)+";";
						i++;
					}
					output.writeObject(message);
					output.flush();
					break;
					
				default:
					break;
				}
			}
			System.out.println("Esce");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally {
			System.out.println("Chiusura connessione");
			try {
				if(socket!=null)
					socket.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			try {
				if(input!=null)
					input.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			try {
				if(output!=null)
					output.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		
	}
	
	public static void main(String[] args) {
		new Server();
	}

}
