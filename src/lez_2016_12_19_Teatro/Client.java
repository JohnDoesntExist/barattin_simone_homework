package lez_2016_12_19_Teatro;

import java.awt.Dimension;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Client {
	
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private Socket socket;
	private String serverMessages;
	private int scelta,nposti;
	private int[] posti, postiDisponibili, check;
	private boolean t1=false;
	
	private JTextArea textArea;
	private JScrollPane scrollPane;
	
	public static void main(String args[]){
		new Client();
	}
	
	Client(){
		try {
			socket=new Socket(InetAddress.getByName("localhost"),54321);
			System.out.println("Connesso con "+socket.getInetAddress().getHostName());
			
			output=new ObjectOutputStream(socket.getOutputStream());
			output.flush();
			
			input=new ObjectInputStream(socket.getInputStream());
			output.writeObject(JOptionPane.showInputDialog("Inserisci il tuo nome"));
			output.flush();
			t1=(boolean)input.readObject();
			JOptionPane.showMessageDialog(null, t1);
			do{
				serverMessages=(String) input.readObject();
				JOptionPane.showMessageDialog(null, serverMessages);
				scelta=Integer.parseInt(JOptionPane.showInputDialog("Scelta"));
				output.writeObject(scelta);
				output.flush();
				switch (scelta) {
				case 0:
					break;
				case 1:
					nposti=Integer.parseInt(JOptionPane.showInputDialog("Numero di posti da prenotare"));
					posti=new int[nposti];
					int i=0;
					while(i<nposti){
						posti[i]=Integer.parseInt(JOptionPane.showInputDialog("Numero del "+(i+1)+"o posto"))-1;
						i++;
					}
					output.writeObject(posti);
					output.flush();
					serverMessages=""+input.readObject();
					
					JOptionPane.showMessageDialog(null, serverMessages);
					if(!serverMessages.equals("Alcuni posti sono gia' occupati. Prenotazione annullata."))
						t1=true;
					break;
					
				case 2:
					output.writeObject(t1);
					output.flush();
					serverMessages=""+input.readObject();
					JOptionPane.showMessageDialog(null, serverMessages);
					break;
					
				case 3:
					output.writeObject(t1);
					output.flush();
					serverMessages=""+input.readObject();
					JOptionPane.showMessageDialog(null, serverMessages);
					break;
					
				case 4:
					serverMessages=""+input.readObject();
					textArea=new JTextArea(serverMessages);
					scrollPane=new JScrollPane(textArea);
					textArea.setLineWrap(true);  
					textArea.setWrapStyleWord(true); 
					scrollPane.setPreferredSize( new Dimension( 300, 300 ) );
					JOptionPane.showMessageDialog(null, scrollPane);
					break;
	
				default:
					break;
				}
			}while(scelta!=0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally {
			System.out.println("Chiusura connessione");
			try {
				if(socket!=null)
					socket.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			try {
				if(output!=null)
					output.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			try {
				if(input!=null)
					input.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
}
