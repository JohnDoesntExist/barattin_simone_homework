package lez_2016_12_05;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

import javax.swing.JOptionPane;

public class Client {
	
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private String message;
	private String server;
	private String s;
	private Socket connection;
	private int tentativi=0, secret; 
	
	public static void main(String args[]){
		new Client();
	}
	
	public Client(){
		try {

			try {
				message = "tentativo di connessione";
				System.out.println(message);
				connection = new Socket( InetAddress.getByName("localhost"), 54321);
				message =  "Connected to: " + 
						connection.getInetAddress().getHostName();
				System.out.println(message);
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				output = new ObjectOutputStream( connection.getOutputStream() );
				output.flush(); 
				input = new ObjectInputStream( connection.getInputStream() );

				message =  "Stream ottenuti";

				int number;
				String result;
				secret=Integer.parseInt(JOptionPane.showInputDialog("Numero da indovinare"));
				output.writeObject(secret);
				do{	
					s=(String)input.readObject();
					JOptionPane.showMessageDialog(null, s);
					if(s.equals("LOST"))
						break;
					number = Integer.parseInt(JOptionPane.showInputDialog("inserire numero"));
					System.out.println("invio numero");
					output.writeObject(number);
					result = (String) input.readObject();
			    	JOptionPane.showMessageDialog(null, result);
					tentativi++;
				}while(tentativi<=10 && result.equals("Errato"));

				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}    

		}   

		finally {
			System.out.println("chiusura connessione");
			try {
				if(connection != null) connection.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				if(output != null) output.close();
			}
			catch( IOException ioException ) {
				ioException.printStackTrace();
			}   


			try {
				if(input != null)input.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
}
