package lez_2016_12_05;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JOptionPane;

public class Server {
	private ObjectOutputStream output1;
	private ObjectInputStream input1;
	private ObjectOutputStream output2;
	private ObjectInputStream input2;
	private ServerSocket server;
	private Socket connection1;
	private Socket connection2;

	private int port = 54321;
	private boolean t=false;
	private int i=0;
	private int secret1,secret2;
	
	public static void main(String agrs[]){
		new Server();
	}
	
	public Server() {
		try {

			server = new ServerSocket(port);

			String message = "server in attesa sulla porta " + port  ;
			System.out.println(message);
			connection1 = server.accept(); 
			connection2 = server.accept();
			
			message =  "Connesso con " +
					connection1.getInetAddress().getHostName();
			System.out.println(message);
			message= "Connesso con "+connection2.getInetAddress().getHostName();
			System.out.println(message);

			output1 = new ObjectOutputStream(connection1.getOutputStream() );
			input1 = new ObjectInputStream(connection1.getInputStream());
			output2 = new ObjectOutputStream(connection2.getOutputStream() );
			input2 = new ObjectInputStream(connection2.getInputStream());

			System.out.println("Stream ottenuti");
			
 			System.out.println("attesa numero");
			secret1=(int) input1.readObject();
			secret2=(int) input2.readObject();
			int n2,n1;
			System.out.println("Numeri letti");
			do{ 
				output1.writeObject("GET");
				output1.flush();
				n1=(int)input1.readObject();
				if(n1==secret2){
					output1.writeObject("WON");
					output1.flush();
					t=true;
					output2.writeObject("LOST");
					output2.flush();
					break;
				}
				else {
					output1.writeObject("Errato");
				}
				output2.writeObject("GET");
				output2.flush();
				n2=(int)input2.readObject();
				if(n2==secret1){
					output2.writeObject("WON");
					output2.flush();
					t=true;
					output1.writeObject("LOST");
					output1.flush();
					break;
				}
				else
					output2.writeObject("Errato");
				if(i==20){
					output1.writeObject("DRAW");
					output2.writeObject("DRAW");
				}
				System.out.println("Tentativo "+i);
				i++;
			}while(t!=true && i<=20);
		}

		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			System.out.println("chiusura connessione");
			try {
				connection1.close();
				connection2.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				if(output1 != null) output1.close();
				if(output2!= null) output2.close();
			}
			catch( IOException ioException ) {
				ioException.printStackTrace();
			}   


			try {
				if(input1 != null)input1.close();
				if(input2 != null)input2.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}


	}
}
