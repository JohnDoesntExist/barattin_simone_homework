package lez_2016_10_15;

class Reader extends Thread{
    SharedZone s;
    Reader(SharedZone s){
        this.s=s;
    }

    @Override
    public void run() {
        super.run();
        while(true) {
            try {
                this.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Class Writer generated the number " + s.getValue());
        }
    }
}
