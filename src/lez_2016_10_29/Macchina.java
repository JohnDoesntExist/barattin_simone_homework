package lez_2016_10_29;

/**
 * Created by simone on 30/10/16.
 */
class Macchina extends Thread{
    Ponte p;
    int dist=0;

    Macchina(Ponte p) {
        this.p=p;
    }

    @Override
    public void run() {
        super.run();
        while(dist!=100){
            try{
                Thread.sleep(1000);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            dist+=10;
            if(dist==50)
                p.entra();
            if(dist==60)
                p.esce();
            System.out.println("La macchina "+Thread.currentThread().getName()+" ha percorso "+dist+"m");
        }
        System.out.println("La macchina "+Thread.currentThread().getName()+" è arrivata.");
    }
}
